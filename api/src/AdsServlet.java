import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by User on 8/5/2016.
 */
@WebServlet(name = "AdsServlet")
public class AdsServlet extends HttpServlet {
    private Connection connection = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("getAds")) {
                final String adImages = getImages();
                if (adImages == null) {
                    response.getWriter().println("No data");
                } else {
                    response.getWriter().println(adImages);
                }
            }
        }
    }

    private void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/dbks", "root", "root");
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getImages() {
        String adImages = null;
        try {
            final PreparedStatement getImagesStatement = connection.prepareStatement("SELECT * FROM tbads");
            final ResultSet resultSet = getImagesStatement.executeQuery();
            final JSONArray jaImages = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joImages = new JSONObject();
                joImages.put("images",resultSet.getString(2));
                jaImages.put(joImages);
            }
            adImages = jaImages.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adImages;
    }
}
