import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by User on 8/10/2016.
 */
@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {
    private Connection connection = null;
    private String gents;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("addAds")) {
                addAds(request);
            } else if (action.equals("deleteAds")) {
                deleteAds(request);
            } else if (action.equals("addNewTrends")) {
                addNewTrends(request);
            } else if (action.equals("deleteNewTrends")) {
                deleteNewTrends(request);
            } else if (action.equals("addToGents")) {
                addToGents(request);
            } else if (action.equals("deleteFromGents")) {
                deleteFromGents(request);
            } else if (action.equals("addToLadies")) {
                addToLadies(request);
            }else if (action.equals("deleteFromLadies")) {
                deleteFromLadies(request);
            } else if (action.equals("addToKids")) {
                addToKids(request);
            }else if (action.equals("deleteFromKids")) {
                deleteFromKids(request);
            }
        } else {
            System.out.println("connection problem");
        }
    }

    private void deleteFromKids(HttpServletRequest request) {
        final String id = request.getParameter("id");
        try {
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tbkids WHERE id=?");
            statement.setInt(1, Integer.parseInt(id));
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteFromLadies(HttpServletRequest request) {
        final String id = request.getParameter("id");
        try {
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tbladies WHERE id=?");
            statement.setInt(1, Integer.parseInt(id));
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteFromGents(HttpServletRequest request) {
        final String id = request.getParameter("id");
        try {
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tbgents WHERE id=?");
            statement.setInt(1, Integer.parseInt(id));
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteNewTrends(HttpServletRequest request) {
        final String id = request.getParameter("id");
        try {
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tbnewtrends WHERE id=?");
            statement.setInt(1, Integer.parseInt(id));
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteAds(HttpServletRequest request) {
        final String name = request.getParameter("name");
        try {
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tbads WHERE name=?");
            statement.setString(1, name);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addToKids(HttpServletRequest request) {
        final String type = request.getParameter("type");
        final String name = request.getParameter("name");
        final String details = request.getParameter("details");
        final String cost = request.getParameter("cost");
        final String image = request.getParameter("image");
        try {
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tbkids(type,name,details,cost,image) VALUES(?,?,?,?,?)");
            statement.setString(1, type);
            statement.setString(2, name);
            statement.setString(3, details);
            statement.setString(4, cost);
            statement.setString(5, image);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addToLadies(HttpServletRequest request) {
        final String type = request.getParameter("type");
        final String name = request.getParameter("name");
        final String details = request.getParameter("details");
        final String cost = request.getParameter("cost");
        final String image = request.getParameter("image");
        try {
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tbladies(type,name,details,cost,image) VALUES(?,?,?,?,?)");
            statement.setString(1, type);
            statement.setString(2, name);
            statement.setString(3, details);
            statement.setString(4, cost);
            statement.setString(5, image);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addToGents(HttpServletRequest request) {
        final String type = request.getParameter("type");
        final String name = request.getParameter("name");
        final String details = request.getParameter("details");
        final String cost = request.getParameter("cost");
        final String image = request.getParameter("image");
        try {
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tbgents(type,name,details,cost,image) VALUES(?,?,?,?,?)");
            statement.setString(1, type);
            statement.setString(2, name);
            statement.setString(3, details);
            statement.setString(4, cost);
            statement.setString(5, image);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addNewTrends(HttpServletRequest request) {
        final String name = request.getParameter("name");
        final String details = request.getParameter("details");
        final String cost = request.getParameter("cost");
        final String image = request.getParameter("image");
        try {
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tbnewtrends(name,details,cost,image) VALUES(?,?,?,?)");
            statement.setString(1, name);
            statement.setString(2, details);
            statement.setString(3, cost);
            statement.setString(4, image);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addAds(HttpServletRequest request) {
        final String name = request.getParameter("name");
        final String image = request.getParameter("image");
        try {
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tbads VALUES(?,?)");
            statement.setString(1, name);
            statement.setString(2, image);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("getAds")) {
                final String result = getAds();
                response.getWriter().println(result);
            } else if (action.equals("getNewTrends")) {
                final String result = getNewTrends();
                response.getWriter().println(result);
            } else if (action.equals("getUsers")) {
                final String result = getUsers();
                response.getWriter().println(result);
            } else if (action.equals("getGents")) {
                final String result = getGents();
                response.getWriter().println(result);
            } else if (action.equals("getLadies")) {
                final String result = getLadies();
                response.getWriter().println(result);
            } else if (action.equals("getKids")) {
                final String result = getKids();
                response.getWriter().println(result);
            }
        } else {
            System.out.println("connection problem");
        }
    }

    private String getKids() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbkids");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("id", resultSet.getString(1));
                joAds.put("type", resultSet.getString(2));
                joAds.put("name", resultSet.getString(3));
                joAds.put("image", resultSet.getString(4));
                joAds.put("details", resultSet.getString(5));
                joAds.put("cost", resultSet.getString(6));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getLadies() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbladies");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("id", resultSet.getString(1));
                joAds.put("type", resultSet.getString(2));
                joAds.put("name", resultSet.getString(3));
                joAds.put("image", resultSet.getString(4));
                joAds.put("details", resultSet.getString(5));
                joAds.put("cost", resultSet.getString(6));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getUsers() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbusers");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("name", resultSet.getString(3));
                joAds.put("phone", resultSet.getString(4));
                joAds.put("mail", resultSet.getString(5));
                joAds.put("address", resultSet.getString(6));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getAds() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbads");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("name", resultSet.getString(1));
                joAds.put("image", resultSet.getString(2));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/dbks", "root", "root");
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getNewTrends() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbnewtrends");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("id", resultSet.getString(1));
                joAds.put("name", resultSet.getString(2));
                joAds.put("details", resultSet.getString(3));
                joAds.put("cost", resultSet.getString(4));
                joAds.put("image", resultSet.getString(5));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getGents() {
        String result = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbgents");
            ResultSet resultSet = statement.executeQuery();
            final JSONArray jaAds = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joAds = new JSONObject();
                joAds.put("id", resultSet.getString(1));
                joAds.put("type", resultSet.getString(2));
                joAds.put("name", resultSet.getString(3));
                joAds.put("image", resultSet.getString(4));
                joAds.put("details", resultSet.getString(5));
                joAds.put("cost", resultSet.getString(6));
                jaAds.put(joAds);
            }
            result = jaAds.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
