import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by User on 8/6/2016.
 */
@WebServlet(name = "GentsServlet")
public class GentsServlet extends HttpServlet {
    private Connection connection = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("getTypes")) {
                final String types = getTypes();
                if (types == null) {
                    response.getWriter().println("No data");
                } else {
                    response.getWriter().println(types);
                }
            } else if (action.equals("getDataByType")) {
                final String type = request.getParameter("type");
                final String data = getData(type);
                if (data == null) {
                    response.getWriter().println("No data");
                } else {
                    response.getWriter().println(data);
                }
            }
        }
    }

    private String getData(String type) {
        String data= null;
        try {
            final  PreparedStatement statement = connection.prepareStatement("SELECT name,details,cost,image FROM tbgents WHERE type=?");
            statement.setString(1,type);
            final ResultSet resultSet  = statement.executeQuery();
            final JSONArray jaData = new JSONArray();
            while (resultSet.next()){
                final JSONObject joData = new JSONObject();
                joData.put("name",resultSet.getString(1));
                joData.put("details",resultSet.getString(2));
                joData.put("cost",resultSet.getString(3));
                joData.put("image",resultSet.getString(4));
                jaData.put(joData);
            }
            data = jaData.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    private void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/dbks", "root", "root");
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getTypes() {
        String types = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT DISTINCT type FROM tbgents");
            final ResultSet resultSet = statement.executeQuery();
            final JSONArray jaTypes = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joTypes = new JSONObject();
                joTypes.put("type", resultSet.getString(1));
                jaTypes.put(joTypes);
            }
            types = jaTypes.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return types;
    }
}
