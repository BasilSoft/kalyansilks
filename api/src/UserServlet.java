import org.json.JSONException;
import org.json.JSONObject;
import sun.misc.Request;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by User on 7/30/2016.
 */
public class UserServlet extends HttpServlet {
    private Connection connection = null;

    private String deleteUserData(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String finalString = null;

        try {
            final PreparedStatement deleteStatement = connection.prepareStatement("Delete FROM tbusers WHERE u_username=? AND u_password=?");
            deleteStatement.setString(1, username);
            deleteStatement.setString(2, password);
            int result = deleteStatement.executeUpdate();
            if (result > 0) {
                finalString = "deleted";
            } else {
                finalString = "notDeleted";
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return finalString;
    }

    private String updateUserData(HttpServletRequest request) {
        String cUsername = request.getParameter("cUsername");
        String cPassword = request.getParameter("cPassword");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String mail = request.getParameter("mail");
        String address = request.getParameter("address");
        String finalResult = null;

        try {

            final PreparedStatement checkStatement = connection.prepareStatement("SELECT * FROM tbusers WHERE (u_username=? AND u_password=?) AND (u_username<>? AND u_password<>?)");
            checkStatement.setString(1, username);
            checkStatement.setString(2, password);
            checkStatement.setString(3, cUsername);
            checkStatement.setString(4, cPassword);

            final ResultSet checkResult = checkStatement.executeQuery();

            if (!checkResult.next()) {
                final PreparedStatement insertStatement = connection.prepareStatement("UPDATE tbusers SET u_username=?,u_password=?,u_name=?,u_phone=?,u_mail=?,u_address=? WHERE u_username=? AND u_password=?");
                insertStatement.setString(1, username);
                insertStatement.setString(2, password);
                insertStatement.setString(3, name);
                insertStatement.setString(4, phone);
                insertStatement.setString(5, mail);
                insertStatement.setString(6, address);
                insertStatement.setString(7, cUsername);
                insertStatement.setString(8, cPassword);

                int result = insertStatement.executeUpdate();
                if (result > 0)
                    finalResult = "updated";
                else
                    finalResult = "notUpdated";

                insertStatement.close();
            } else {
                finalResult = "exists";
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return finalResult;
    }

    private String addUserData(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String mail = request.getParameter("mail");
        String address = request.getParameter("address");
        String finalResult = null;
        try {
            final PreparedStatement checkStatement = connection.prepareStatement("SELECT * FROM tbusers WHERE u_username=? AND u_password=?");
            checkStatement.setString(1, username);
            checkStatement.setString(2, password);

            final ResultSet checkResult = checkStatement.executeQuery();
            if (!checkResult.next()) {
                final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO tbusers(u_username,u_password,u_name,u_phone,u_mail,u_address) VALUES(?,?,?,?,?,?)");
                insertStatement.setString(1, username);
                insertStatement.setString(2, password);
                insertStatement.setString(3, name);
                insertStatement.setString(4, phone);
                insertStatement.setString(5, mail);
                insertStatement.setString(6, address);
                int result = insertStatement.executeUpdate();
                if (result > 0)
                    finalResult = "inserted";
                else
                    finalResult = "notInserted";
                insertStatement.close();
            } else {
                finalResult = "exists";
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return finalResult;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("userAccess")) {
                final String message = getMessage(request);
                response.getWriter().println(message);
            } else if (action.equals("getUserData")) {
                final String userData = getUserData(request);
                response.getWriter().println(userData);
            } else if (action.equals("addUserData")) {
                final String result = addUserData(request);
                if (result != null) {
                    response.getWriter().println(result);
                }
            } else if (action.equals("updateUserData")) {
                final String result = updateUserData(request);
                if (result != null) {
                    response.getWriter().println(result);
                }
            } else if (action.equals("deleteUserData")) {
                final String result = deleteUserData(request);
                if (result != null) {
                    response.getWriter().println(result);
                }
            }
        } else {
            System.out.println("connection problem");
        }


    }

    private String getUserData(HttpServletRequest request) {
        final String username = request.getParameter("username");
        final String password = request.getParameter("password");
        String userData = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbusers WHERE u_username=? AND  u_password=?");
            statement.setString(1, username);
            statement.setString(2, password);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final String name = resultSet.getString(3);
                final String phone = resultSet.getString(4);
                final String mail = resultSet.getString(5);
                final String address = resultSet.getString(6);
                final JSONObject joUserData = new JSONObject();
                joUserData.put("name", name);
                joUserData.put("phone", phone);
                joUserData.put("mail", mail);
                joUserData.put("address",address);
                userData = joUserData.toString();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userData;
    }

    // it returns allow or reject message by checking username or password
    private String getMessage(HttpServletRequest request) {
        final String username = request.getParameter("username");
        final String password = request.getParameter("password");
        String message = null;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT  * FROM tbusers WHERE u_username=? AND  u_password=?");
            statement.setString(1, username);
            statement.setString(2, password);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                message = "allow";
            } else {
                message = "reject";
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return message;
    }

    private void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/dbks", "root", "root");
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
