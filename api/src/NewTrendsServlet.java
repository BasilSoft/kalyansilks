import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

/**
 * Created by User on 8/4/2016.
 */
@WebServlet(name = "NewTrendsServlet")
public class NewTrendsServlet extends HttpServlet {
    private Connection connection = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        getConnection();
//
//        File file = new File("D:\\shirt.jpeg");
//        FileInputStream stream = new FileInputStream(file);
//        byte[] bytes = new byte[(int) file.length()];
//        stream.read(bytes);
//        String a = Base64.encode(bytes);
//
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO tbkids(type,name,details,cost,image) VALUES(?,?,?,?,?)");
//            preparedStatement.setString(1,"shirt");
//            preparedStatement.setString(2, "new jen");
//            preparedStatement.setString(3, "new model shirt");
//            preparedStatement.setString(4, "410");
//            preparedStatement.setString(5, a);
//            preparedStatement.executeUpdate();
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getConnection();

        if (connection != null) {
            final String action = request.getParameter("action");
            if (action.equals("getNewTrends")) {
                final String newTrends = getNewTrends();
                if (newTrends == null) {
                    response.getWriter().println("No data");
                } else {
                    response.getWriter().println(newTrends);
                }
            }
        }
    }

    private String getNewTrends() {
        String newTrends = null;
        try {
            final PreparedStatement newTrendsStatement = connection.prepareStatement("SELECT * FROM tbnewtrends");
            final ResultSet resultSet = newTrendsStatement.executeQuery();
            final JSONArray jaNewTrends = new JSONArray();
            while (resultSet.next()) {
                final JSONObject joNewTrends = new JSONObject();
                joNewTrends.put("name", resultSet.getString(2));
                joNewTrends.put("details", resultSet.getString(3));
                joNewTrends.put("cost", resultSet.getString(4));
                joNewTrends.put("image", resultSet.getString(5));
                jaNewTrends.put(joNewTrends);
            }
            newTrends = jaNewTrends.toString();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newTrends;
    }

    private void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/dbks", "root", "root");
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
