package ks.client.classes.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;
import ks.client.classes.activities.SignInActivity;
import ks.client.classes.adapters.ShowItemsAdapter;
import ks.client.classes.models.Product;

/**
 * Created by User on 7/30/2016.
 */
public class SimpleFragment extends Fragment {
    Context context;
    String title;
    String type;
    public static RecyclerView rvContent;
    public static ShowItemsAdapter showItemsAdapter;

    public SimpleFragment() {
    }

    public SimpleFragment(final Context context, final String title, final String type) {
        this.context = context;
        this.title = title;
        this.type = type;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment, container, false);
        rvContent = (RecyclerView) v.findViewById(R.id.rvContent);
        rvContent.setLayoutManager(new LinearLayoutManager(context));

        final List<Product> products = getProductsByType(title);
//        showItemsAdapter = new ShowItemsAdapter(context, products);
        rvContent.setAdapter(showItemsAdapter);
        return v;
    }

    private List<Product> getProductsByType(String title) {
        final List<Product> products = new ArrayList<>();
        final String url = SignInActivity.BASIC_URL + type + "?action=getDataByType&type=" + title;
        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);
        final ProgressDialog loadingDialog = ProgressDialog.show(context, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(context, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final JSONArray jaData = new JSONArray(resp);
                        for (int i = 0; i < jaData.length(); i++) {
                            final JSONObject joData = jaData.getJSONObject(i);
                            final String name = joData.getString("name");
                            final String details = joData.getString("details");
                            final String cost = joData.getString("cost");
                            final String image = joData.getString("image");
                            products.add(new Product(name, details, cost, image));
                        }
                       showItemsAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();

        return products;
    }
}
