package ks.client.classes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ks.client.R;
import ks.client.classes.activities.ShowItemsActivity;
import ks.client.classes.adapters.ShowItemsAdapter;

/**
 * Created by User on 8/8/2016.
 */
public class ViewItemsFragment extends Fragment {
    ShowItemsAdapter showItemsAdapter;
    public static RecyclerView rvContent;
    Context context;
    int position;
    String type;
    String title;

    public ViewItemsFragment(Context context, int position, String type, String title) {
        this.context = context;
        this.position = position;
        this.type = type;
        this.title = title;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment, container, false);
        rvContent = (RecyclerView) v.findViewById(R.id.rvContent);
        rvContent.setLayoutManager(new LinearLayoutManager(context));

        ShowItemsActivity.rvContents[position] = rvContent;
        showItemsAdapter = new ShowItemsAdapter(type,title,context,ShowItemsActivity.products[position]);
        rvContent.setAdapter(showItemsAdapter);
        ShowItemsActivity.showItemsAdapters[position] = showItemsAdapter;

        return v;
    }

}
