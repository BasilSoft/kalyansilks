package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;

/**
 * Created by User on 8/2/2016.
 */
public class CreateNewActivity extends AppCompatActivity implements View.OnClickListener {
    Button bSave;
    EditText etUsername, etPassword, etRetypePassword, etName, etPhone, etMail, etAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new);

        initializeViews();

        bSave.setOnClickListener(this);
    }

    private void initializeViews() {
        bSave = (Button) findViewById(R.id.bSave);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRetypePassword = (EditText) findViewById(R.id.etRetypePassword);
        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etMail = (EditText) findViewById(R.id.etMail);
        etAddress = (EditText) findViewById(R.id.etAddress);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSave:
                checkDataAndSave();
                break;
        }
    }

    private void checkDataAndSave() {
        final String username = etUsername.getText().toString();
        final String password = etPassword.getText().toString();
        final String name = etName.getText().toString();
        final String phone = etPhone.getText().toString();
        final String mail = etMail.getText().toString();
        final String address = etAddress.getText().toString();
        if (!password.equals(etRetypePassword.getText().toString()) || username.equals("") || password.equals("") || name.equals("") || phone.equals("") || mail.equals("") || address.equals("")) {
            Toast.makeText(this, "Please fill all fields or Note match password", Toast.LENGTH_SHORT).show();
        } else {
            doSave(username, password, name, phone, mail, address);
        }
    }

    private void doSave(String username, String password, String name, String phone, String mail, String address) {

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "addUserData"));
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("mail", mail));
        params.add(new BasicNameValuePair("address", address));

        final String paramString = URLEncodedUtils.format(params, "utf-8");
        String url = SignInActivity.BASIC_URL + "user";
        url += "?" + paramString;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(CreateNewActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(CreateNewActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    if (resp.contains("exist")) {
                        Toast.makeText(CreateNewActivity.this, "Username or password already exists !", Toast.LENGTH_SHORT).show();
                    } else if (resp.contains("inserted")) {
                        Toast.makeText(CreateNewActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (resp.contains("notInserted")) {
                        Toast.makeText(CreateNewActivity.this, "Not Saved", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }.execute();

    }
}
