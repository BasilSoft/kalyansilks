package ks.client.classes.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by User on 7/26/2016.
 */
public class BitmapUtils {
    public Bitmap decodeImage(String imageString){
        byte[] base64ProfilePic = Base64.decode(imageString, Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        final Bitmap profilePic = BitmapFactory.decodeByteArray(base64ProfilePic, 0, base64ProfilePic.length, options);
        return profilePic;
    }
}
