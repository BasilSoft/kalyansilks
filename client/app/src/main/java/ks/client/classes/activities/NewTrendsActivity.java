package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ks.client.R;
import ks.client.classes.adapters.NewTrendsAdapter;
import ks.client.classes.models.Trend;

/**
 * Created by User on 8/2/2016.
 */
public class NewTrendsActivity extends AppCompatActivity implements View.OnClickListener {
    Button bOptions;
    RecyclerView rvContent;
    List<Trend> trendses = new ArrayList<>();
    NewTrendsAdapter trendsAdapter;
    //it is used to get state of sort such as ascending or descending
    String sortState = "ascending";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trends);

        initializeViews();
        trendsAdapter = new NewTrendsAdapter(this, trendses);

        rvContent.setLayoutManager(new LinearLayoutManager(this));
        rvContent.setAdapter(trendsAdapter);
        bOptions.setOnClickListener(this);

        loadNewTrends();
    }

    private void loadNewTrends() {
        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "getNewTrends"));

        final String paramValues = URLEncodedUtils.format(params, "utf-8");
        String url = SignInActivity.BASIC_URL + "newt";
        url += "?" + paramValues;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(NewTrendsActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(NewTrendsActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    loadJSONData(resp);
                }
            }
        }.execute();

    }

    private void loadJSONData(String resp) {
        try {
            final JSONArray jaNewTrends = new JSONArray(resp);
            for (int i = 0; i < jaNewTrends.length(); i++) {
                final JSONObject joNewTrends = jaNewTrends.getJSONObject(i);
                final String name = joNewTrends.getString("name");
                final String details = joNewTrends.getString("details");
                final String cost = joNewTrends.getString("cost");
                final String image = joNewTrends.getString("image");
                trendses.add(new Trend(name, details, cost, image));
            }
            trendsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initializeViews() {
        bOptions = (Button) findViewById(R.id.bOptions);
        rvContent = (RecyclerView) findViewById(R.id.rvContent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bOptions:
                showOptions();
                break;
        }
    }

    private void showOptions() {
        View anchor = bOptions;
        PopupMenu popupMenu = new PopupMenu(this, anchor);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.miName:
                        sortByName();
                        break;
                    case R.id.miPrize:
                        sortByPrize();
                        break;
                    case R.id.miPrizeInterval:
                        doIntervalSort();
                        break;
                }
                return false;
            }

            private void doIntervalSort() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(NewTrendsActivity.this);
                builder.setTitle("Enter limit");
                View v = LayoutInflater.from(NewTrendsActivity.this).inflate(R.layout.layout_interval, null, false);
                builder.setView(v);
                final EditText etStart = (EditText) v.findViewById(R.id.etStart);
                final EditText etEnd = (EditText) v.findViewById(R.id.etEnd);
                final Button bSort = (Button) v.findViewById(R.id.bSort);
                final AlertDialog alert = builder.show();
                bSort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            final Integer startValue = Integer.parseInt(etStart.getText().toString());
                            final Integer endValue = Integer.parseInt(etEnd.getText().toString());
                            List<Trend> sorted = new ArrayList<>();
                            for (int i = 0; i < trendses.size(); i++) {
                                final Trend trends = trendses.get(i);
                                final int cost = Integer.valueOf(trends.getCost());
                                if (cost >= startValue && cost <= endValue) {
                                    sorted.add(trends);
                                }
                            }
                            rvContent.setAdapter(new NewTrendsAdapter(NewTrendsActivity.this, sorted));
                            alert.dismiss();
                        }catch (Exception e){
                            Toast.makeText(NewTrendsActivity.this, "Enter inputs", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }

            private void sortByPrize() {
                if (sortState.equals("ascending")) {
                    sortState = "descending";
                    Collections.sort(trendses, new Comparator<Trend>() {
                        @Override
                        public int compare(Trend lhs, Trend rhs) {
                            return Integer.valueOf(lhs.getCost()).compareTo(Integer.valueOf(rhs.getCost()));
                        }
                    });
                } else {
                    sortState = "ascending";
                    Collections.sort(trendses, new Comparator<Trend>() {
                        @Override
                        public int compare(Trend lhs, Trend rhs) {
                            return Integer.valueOf(rhs.getCost()).compareTo(Integer.valueOf(lhs.getCost()));
                        }
                    });
                }
                trendsAdapter.notifyDataSetChanged();
            }

            private void sortByName() {
                if (sortState.equals("ascending")) {
                    sortState = "descending";
                    Collections.sort(trendses, new Comparator<Trend>() {
                        @Override
                        public int compare(Trend lhs, Trend rhs) {
                            return lhs.getName().compareToIgnoreCase(rhs.getName());
                        }
                    });
                } else {
                    sortState = "ascending";
                    Collections.sort(trendses, new Comparator<Trend>() {
                        @Override
                        public int compare(Trend lhs, Trend rhs) {
                            return rhs.getName().compareToIgnoreCase(lhs.getName());
                        }
                    });
                }
                trendsAdapter.notifyDataSetChanged();
            }
        });
    }
}
