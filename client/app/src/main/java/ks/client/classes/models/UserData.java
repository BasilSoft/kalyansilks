package ks.client.classes.models;

/**
 * Created by User on 8/10/2016.
 */
public class UserData {
    final String name;
    final String phone;
    final String mail;
    final String address;

    public UserData(String name, String phone, String mail, String address) {
        this.name = name;
        this.phone = phone;
        this.mail = mail;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getMail() {
        return mail;
    }

    public String getAddress() {
        return address;
    }
}
