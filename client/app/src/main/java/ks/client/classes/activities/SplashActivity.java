package ks.client.classes.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ks.client.R;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent iStartSignInActivity  = new Intent(SplashActivity.this,SignInActivity.class);
                startActivity(iStartSignInActivity);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
}
