package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;

/**
 * Created by User on 7/29/2016.
 */
public class SettingsActivity extends AppCompatActivity {
    Button bDelete, bChange;
    EditText etUsername, etPassword, etRetypePassword, etName, etPhone, etMail, etAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initializeViews();

        bChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                final String username = extras.getString("KEYUsername");
                final String password = extras.getString("KEYPassword");

                final String newUsername = etUsername.getText().toString();
                final String newPassword = etPassword.getText().toString();
                final String reyPassword = etRetypePassword.getText().toString();
                final String name = etName.getText().toString();
                final String phone = etPhone.getText().toString();
                final String mail = etMail.getText().toString();
                final String address = etAddress.getText().toString();

                if (!newPassword.equals(reyPassword) || newUsername.equals("") || newPassword.equals("") || name.equals("") || phone.equals("") || mail.equals("") || address.equals("")) {
                    Toast.makeText(SettingsActivity.this, "Please fill the form or password not match !", Toast.LENGTH_SHORT).show();
                } else {
                    doChange(username, password, newUsername, newPassword, name, phone, mail, address);
                }
            }
        });
        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle("Confirm");
                builder.setMessage("Are you sure to delete account ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle extras = getIntent().getExtras();
                        final String username = extras.getString("KEYUsername");
                        final String password = extras.getString("KEYPassword");
                        doDelete(username, password);
                    }
                });
                builder.setNegativeButton("No",null);
                builder.show();
            }
        });
    }

    private void doDelete(String username, String password) {

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "deleteUserData"));
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));

        final String paramValues = URLEncodedUtils.format(params, "utf-8");
        String url = SignInActivity.BASIC_URL + "user";
        url += "?" + paramValues;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(SettingsActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(SettingsActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    if (resp.contains("deleted")) {
                        Toast.makeText(SettingsActivity.this, "Your account has been deleted", Toast.LENGTH_SHORT).show();
                        HomeActivity.homeActivityInstance.finish();
                        finish();
                    } else if (resp.contains("notDeleted")) {
                        Toast.makeText(SettingsActivity.this, "Can't delete your account !", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }.execute();

    }

    private void initializeViews() {
        bDelete = (Button) findViewById(R.id.bDelete);
        bChange = (Button) findViewById(R.id.bChange);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRetypePassword = (EditText) findViewById(R.id.etRetypePassword);
        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etMail = (EditText) findViewById(R.id.etMail);
        etAddress = (EditText) findViewById(R.id.etAddress);
    }

    private void doChange(String username, String password, String newUsername, String newPassword, String name, String phone, String mail, String address) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "updateUserData"));
        params.add(new BasicNameValuePair("cUsername", username));
        params.add(new BasicNameValuePair("cPassword", password));
        params.add(new BasicNameValuePair("username", newUsername));
        params.add(new BasicNameValuePair("password", newPassword));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("mail", mail));
        params.add(new BasicNameValuePair("address", address));

        final String paramValues = URLEncodedUtils.format(params, "utf-8");

        String url = SignInActivity.BASIC_URL + "user";
        url += "?" + paramValues;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(SettingsActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(SettingsActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    if (resp.contains("exist")) {
                        Toast.makeText(SettingsActivity.this, "Username or password already exists !", Toast.LENGTH_SHORT).show();
                    } else if (resp.contains("updated")) {
                        Toast.makeText(SettingsActivity.this, "Changed", Toast.LENGTH_SHORT).show();
                        finish();

                    } else if (resp.contains("notUpdated")) {
                        Toast.makeText(SettingsActivity.this, "Not Changed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }.execute();

    }
}
