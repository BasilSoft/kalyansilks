package ks.client.classes.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import ks.client.classes.fragments.ViewItemsFragment;

/**
 * Created by User on 8/8/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter  {
    final List<String> typeList;
    final Context context;
    final String type;

    public ViewPagerAdapter(Context context, FragmentManager fm,String type, List<String> typeList) {
        super(fm);
        this.typeList = typeList;
        this.context = context;
        this.type = type;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return typeList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return new ViewItemsFragment(context,position,type,typeList.get(position));
    }

    @Override
    public int getCount() {
        return typeList.size();
    }

}
