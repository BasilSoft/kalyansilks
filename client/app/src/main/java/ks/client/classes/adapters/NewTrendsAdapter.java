package ks.client.classes.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ks.client.R;
import ks.client.classes.activities.ViewActivity;
import ks.client.classes.models.Trend;
import ks.client.classes.utils.BitmapUtils;

/**
 * Created by User on 8/2/2016.
 */
public class NewTrendsAdapter extends RecyclerView.Adapter<NewTrendsAdapter.viewHolder> {
    public List<Trend> trends;
    Context context;

    public NewTrendsAdapter(Context context, List<Trend> trends) {
        this.context = context;
        this.trends = trends;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.product_item, parent, false);
        return new viewHolder(v);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        final Trend trends = this.trends.get(position);
        holder.bView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iViewActivity = new Intent(context, ViewActivity.class);
                iViewActivity.putExtra("KEYImage",trends.getImage());
                iViewActivity.putExtra("KEYCost", trends.getCost());
                iViewActivity.putExtra("KEYId","New trends");
                iViewActivity.putExtra("KEYName",trends.getName());
                context.startActivity(iViewActivity);
            }
        });
        holder.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Details");
                builder.setMessage(trends.getDetails());
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        holder.tvName.setText(trends.getName());
        holder.tvCost.setText(trends.getCost());

        BitmapUtils bitmapUtils = new BitmapUtils();
        holder.ivProduct.setImageBitmap(bitmapUtils.decodeImage(trends.getImage()));
    }

    @Override
    public int getItemCount() {
        return trends.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        Button bView;
        TextView tvName, tvCost, tvMore;
        ImageView ivProduct;

        public viewHolder(View itemView) {
            super(itemView);
            bView = (Button) itemView.findViewById(R.id.bView);
            tvName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvCost = (TextView) itemView.findViewById(R.id.tvProductCost);
            tvMore = (TextView) itemView.findViewById(R.id.tvProductDetails);
            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);
        }
    }
}
