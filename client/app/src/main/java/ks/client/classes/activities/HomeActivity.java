package ks.client.classes.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;
import ks.client.classes.models.UserData;
import ks.client.classes.utils.BitmapUtils;

/**
 * Created by User on 7/29/2016.
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    public static Activity homeActivityInstance;
    public static UserData userData;
    RelativeLayout rlForGents, rlForLadies, rlForKids;
    Button bOpenMenu;
    TextView tvForGentsHelp, tvForLadiesHelp, tvForKidsHelp, tvName, tvMail, tvPhone;
    RelativeLayout rlAboutUs, rlContactUs, rlNewTrends, rlSettings, rlSignOut;
    ImageView ivAds;
    DrawerLayout dlMain;
    int animListCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homeActivityInstance = this;

        initializeViews();

        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final String username = extras.getString("KEYUsername");
            final String password = extras.getString("KEYPassword");
            loadProfileAndAds(username, password);
        }


        bOpenMenu.setOnClickListener(this);
        rlForGents.setOnClickListener(this);
        rlForLadies.setOnClickListener(this);
        rlForKids.setOnClickListener(this);
        tvForGentsHelp.setOnClickListener(this);
        tvForLadiesHelp.setOnClickListener(this);
        tvForKidsHelp.setOnClickListener(this);
        rlAboutUs.setOnClickListener(this);
        rlContactUs.setOnClickListener(this);
        rlSignOut.setOnClickListener(this);
        rlNewTrends.setOnClickListener(this);
        rlSettings.setOnClickListener(this);

    }

    private Bitmap getBitmap(List<String> images, int i) {
        BitmapUtils bitmapUtils = new BitmapUtils();
        return bitmapUtils.decodeImage(images.get(i));
    }

    private void initializeViews() {

        dlMain = (DrawerLayout) findViewById(R.id.dlMain);
        ivAds = (ImageView) findViewById(R.id.ivAds);

        rlForGents = (RelativeLayout) findViewById(R.id.rlForGents);
        rlForLadies = (RelativeLayout) findViewById(R.id.rlForLadies);
        rlForKids = (RelativeLayout) findViewById(R.id.rlForKids);
        bOpenMenu = (Button) findViewById(R.id.bOpenMenu);

        tvForGentsHelp = (TextView) findViewById(R.id.tvForGentsHelp);
        tvForLadiesHelp = (TextView) findViewById(R.id.tvForLadiesHelp);
        tvForKidsHelp = (TextView) findViewById(R.id.tvForKidsHelp);
        tvName = (TextView) findViewById(R.id.tvName);
        tvMail = (TextView) findViewById(R.id.tvMail);
        tvPhone = (TextView) findViewById(R.id.tvPhone);

        rlAboutUs = (RelativeLayout) findViewById(R.id.rlAboutUs);
        rlContactUs = (RelativeLayout) findViewById(R.id.rlContactUs);
        rlNewTrends = (RelativeLayout) findViewById(R.id.rlNewTrend);
        rlSignOut = (RelativeLayout) findViewById(R.id.rlSignOut);
        rlSettings = (RelativeLayout) findViewById(R.id.rlSettings);

    }

    private void loadProfileAndAds(String username, String password) {
        final String adsUrl = SignInActivity.BASIC_URL + "ads?action=getAds";

        List<NameValuePair> userParams = new ArrayList<>();
        userParams.add(new BasicNameValuePair("action", "getUserData"));
        userParams.add(new BasicNameValuePair("username", username));
        userParams.add(new BasicNameValuePair("password", password));

        final String userParamString = URLEncodedUtils.format(userParams, "utf-8");
        String userUrl = SignInActivity.BASIC_URL + "user";
        userUrl += "?" + userParamString;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet userHttpGet = new HttpGet(userUrl);
        final HttpGet adsHttpGet = new HttpGet(adsUrl);

        final ProgressDialog loadingDialog = ProgressDialog.show(HomeActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String userResp = null;
            String adsResp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse userResponse = client.execute(userHttpGet);
                    HttpResponse adsResponse = client.execute(adsHttpGet);
                    HttpEntity userHttpEntity = userResponse.getEntity();
                    HttpEntity adsHttpEntity = adsResponse.getEntity();
                    userResp = EntityUtils.toString(userHttpEntity, "utf-8");
                    adsResp = EntityUtils.toString(adsHttpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (adsResp == null) {
                    Toast.makeText(HomeActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final List<String> images = new ArrayList<>();
                        final JSONArray jaImages = new JSONArray(adsResp);
                        for (int i = 0; i < jaImages.length(); i++) {
                            final JSONObject joImages = jaImages.getJSONObject(i);
                            images.add(joImages.getString("images"));
                        }
                        loadImages(images);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (userResp == null) {
                    Toast.makeText(HomeActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final JSONObject joUser = new JSONObject(userResp);
                        final String name = joUser.getString("name");
                        final String phone = joUser.getString("phone");
                        final String mail = joUser.getString("mail");
                        final String address = joUser.getString("address");
                        tvName.setText(name);
                        tvPhone.setText(phone);
                        tvMail.setText(mail);
                        userData = new UserData(name,phone,mail,address);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            private void loadImages(final List<String> images) {
                final Animation animShow = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.anim_show);
                final Animation animHide = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.anim_hide);

                ivAds.setAnimation(animShow);
                animShow.start();
                animShow.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if (animListCount >= images.size()) {
                            animListCount = 0;
                        }
                        ivAds.setImageBitmap(getBitmap(images, animListCount++));
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        ivAds.setAnimation(animHide);
                        animHide.start();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                animHide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        ivAds.setAnimation(animShow);
                        animShow.start();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlForGents:
                showItems("gents");
                break;
            case R.id.rlForLadies:
                showItems("ladies");
                break;
            case R.id.rlForKids:
                showItems("kids");
                break;
            case R.id.rlAboutUs:
                final String aboutMessage = "Kalyan Silks was founded in Thrissur in 1909 as a store specializing in silk. Kalyan silks is The World Largest Silks Saree Showroom Network. We have 21 showrooms in India and the UAE";
                new AlertDialog.Builder(this).setTitle("About us").setMessage(aboutMessage).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
                break;
            case R.id.rlContactUs:
                final String contactMessage = "\n" +
                        "Corporate office\n" +
                        "\n" +
                        "Kuriachira, Thrissur 680 006\n" +
                        "Tel: +91 487 2253975, 2253969\n" +
                        "Fax: +91 487 2253971\n" +
                        "\n" +
                        "E-mail: customerservice@kalyansilks.com\n";
                new AlertDialog.Builder(this).setTitle("Contact us").setMessage(contactMessage).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
                break;
            case R.id.rlSettings:
                Intent iSettingsAccessActivity = new Intent(HomeActivity.this, SettingAccessActivity.class);
                startActivity(iSettingsAccessActivity);
                break;
            case R.id.rlSignOut:
                finish();
                break;
            case R.id.rlNewTrend:
                Intent iNewTrendsActivity = new Intent(HomeActivity.this, NewTrendsActivity.class);
                startActivity(iNewTrendsActivity);
                break;
            case R.id.bOpenMenu:
                dlMain.openDrawer(Gravity.LEFT);
                break;
            case R.id.tvForGentsHelp:
                final String gentsHelpMessage = "You can see and buy dresses for all gents above 1 year old";
                new AlertDialog.Builder(this).setTitle("About us").setMessage(gentsHelpMessage).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
                break;
            case R.id.tvForLadiesHelp:
                final String ladiesHelpMessage = "You can see and buy dresses for all ladies above 1 year old";
                new AlertDialog.Builder(this).setTitle("About us").setMessage(ladiesHelpMessage).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
                break;
            case R.id.tvForKidsHelp:
                final String kidsHelpMessage = "You can see and buy dresses for all kids below 1 year old";
                new AlertDialog.Builder(this).setTitle("About us").setMessage(kidsHelpMessage).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
                break;
        }
    }

    private void showItems(String type) {
        Intent iShowItemsActivity = new Intent(HomeActivity.this, ShowItemsActivity.class);
        iShowItemsActivity.putExtra("KEYType", type);
        startActivity(iShowItemsActivity);
    }
}
