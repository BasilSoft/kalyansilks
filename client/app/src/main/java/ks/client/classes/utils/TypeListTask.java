package ks.client.classes.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import ks.client.classes.activities.ShowItemsActivity;
import ks.client.classes.activities.SignInActivity;
import ks.client.classes.models.Product;

/**
 * Created by User on 8/9/2016.
 */
public class TypeListTask extends AsyncTask<List<Product>, List<Product>, List<Product>> {
    final DefaultHttpClient client;
    List<String> typeList;
    String type;
    Context context;
    int currentPage;

    public TypeListTask(int currentPage, Context context, List<String> typeList, String type) {
        this.context = context;
        this.typeList = typeList;
        this.type = type;
        client = new DefaultHttpClient();
        this.currentPage = currentPage;
    }

    @Override
    protected List<Product> doInBackground(List<Product>... params) {
        for (int typeI = 0; typeI < typeList.size(); typeI++) {
            String url = SignInActivity.BASIC_URL + type + "?action=getDataByType&type=" + typeList.get(typeI);
            HttpGet httpGet = new HttpGet(url);
            try {
                HttpResponse response = client.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                String resp = EntityUtils.toString(httpEntity, "utf-8");
                if (resp == null) {
                    Toast.makeText(context, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final JSONArray jaData = new JSONArray(resp);
                        for (int contentI = 0; contentI < jaData.length(); contentI++) {
                            final JSONObject joData = jaData.getJSONObject(contentI);
                            final String name = joData.getString("name");
                            final String details = joData.getString("details");
                            final String cost = joData.getString("cost");
                            final String image = joData.getString("image");
                            ShowItemsActivity.products[typeI].add(new Product(name, details, cost, image));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Product> products) {
        try {
            ShowItemsActivity.showItemsAdapters[currentPage].notifyDataSetChanged();
        } catch (Exception e) {

        }
    }
}
