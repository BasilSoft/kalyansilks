package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;

/**
 * Created by User on 7/29/2016.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String BASIC_URL = "http://192.168.0.107" + ":8080/";
    Button bSignIn;
    EditText etUsername, etPassword;
    TextView tvCreate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        initializeViews();

        bSignIn.setOnClickListener(this);
        tvCreate.setOnClickListener(this);

    }

    private void initializeViews() {
        bSignIn = (Button) findViewById(R.id.bSignIn);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvCreate = (TextView) findViewById(R.id.tvCreate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSignIn:
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();
                checkUserAndSignIn(username, password);
                break;
            case R.id.tvCreate:
                Intent iCreateNewActivity = new Intent(SignInActivity.this, CreateNewActivity.class);
                startActivity(iCreateNewActivity);
                break;
        }
    }

    private void checkUserAndSignIn(final String username, final String password) {

        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "userAccess"));
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        final String paramString = URLEncodedUtils.format(params, "utf-8");
        String url = BASIC_URL + "user";
        url += "?" + paramString;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(SignInActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(SignInActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    if (resp.contains("allow")) {
                        doSignIn();
                    } else if (resp.contains("reject")) {
                        Toast.makeText(SignInActivity.this, "Invalid username or password !", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            private void doSignIn() {
                Intent iHomeActivity = new Intent(SignInActivity.this, HomeActivity.class);
                iHomeActivity.putExtra("KEYUsername", username);
                iHomeActivity.putExtra("KEYPassword", password);
                startActivity(iHomeActivity);
                finish();
            }
        }.execute();
    }
}
