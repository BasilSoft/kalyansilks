package ks.client.classes.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ks.client.R;
import ks.client.classes.models.UserData;
import ks.client.classes.utils.BitmapUtils;

/**
 * Created by User on 8/2/2016.
 */
public class ViewActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView ivProduct;
    TextView tvCostRs;
    Button bSend;
    Spinner sSizes;
    String realCost;
    private String id;
    private String pName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        initializeViews();

        Intent intent = getIntent();
        if (intent != null) {
            final String cost = intent.getStringExtra("KEYCost");
            final String image = intent.getStringExtra("KEYImage");
            id = intent.getStringExtra("KEYId");
            pName = intent.getStringExtra("KEYName");
            BitmapUtils bitmapUtils = new BitmapUtils();
            ivProduct.setImageBitmap(bitmapUtils.decodeImage(image));
            tvCostRs.setText(cost);
            realCost = cost;
        }

        bSend.setOnClickListener(this);

        List<String> items = new ArrayList<>();
        items.add("Small");
        items.add("Medium");
        items.add("Large");
        items.add("Extra Large");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, items);
        sSizes.setAdapter(adapter);
        sSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = parent.getItemAtPosition(position).toString();
                int cost = Integer.valueOf(realCost);

                if (value.equals("Small")) {
                    tvCostRs.setText(String.valueOf(cost - (cost * 30 / 100)));
                } else if (value.equals("Medium")) {
                    tvCostRs.setText(realCost);
                } else if (value.equals("Large")) {
                    tvCostRs.setText(String.valueOf(cost + (cost * 30 / 100)));
                } else if (value.equals("Extra Large")) {
                    tvCostRs.setText(String.valueOf(cost + (cost * 60 / 100)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initializeViews() {
        bSend = (Button) findViewById(R.id.bSend);
        ivProduct = (ImageView) findViewById(R.id.ivProduct);
        tvCostRs = (TextView) findViewById(R.id.tvCostRs);
        sSizes = (Spinner) findViewById(R.id.sSizes);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSend:
                final String size = sSizes.getSelectedItem().toString();
                sendMail(size);
                break;
        }
    }

    private void sendMail(String size) {
        final UserData userData = HomeActivity.userData;
        final String name = userData.getName();
        final String phone = userData.getPhone();
        final String mail = userData.getMail();
        final String address = userData.getAddress();
        final String MESSAGE = "Enquiry from:\n\n" + name + "\n" + phone + "\n" + mail + "\n" + address + "\n\nSelected Product:\n\n" + id + "->" + pName + "->" + size;

        Intent iMailSend = new Intent(Intent.ACTION_SENDTO);
        iMailSend.setType("text/plain");
        iMailSend.setData(Uri.parse("mailto:basil@rdxcompany.com"));
        iMailSend.putExtra(Intent.EXTRA_SUBJECT, "Enquiry from customer");
        iMailSend.putExtra(Intent.EXTRA_TEXT, MESSAGE);
        startActivity(iMailSend);
    }
}
