package ks.client.classes.models;

/**
 * Created by User on 8/6/2016.
 */
public class Product {
    final String name, details, cost, image;

    public Product(String name, String details, String cost, String image) {
        this.name = name;
        this.details = details;
        this.cost = cost;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }

    public String getCost() {
        return cost;
    }

    public String getImage() {
        return image;
    }
}
