package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ks.client.R;

/**
 * Created by User on 7/29/2016.
 */
public class SettingAccessActivity extends AppCompatActivity {
    EditText etUsername, etPassword;
    Button bGo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_access);

        initializeViews();

        bGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();

                checkAndGo(username, password);
            }
        });

    }

    private void checkAndGo(final String username, final String password) {
        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("action", "userAccess"));
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));

        final String paramString = URLEncodedUtils.format(params, "utf-8");
        String url = SignInActivity.BASIC_URL + "user";
        url += "?" + paramString;

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(SettingAccessActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(SettingAccessActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    if (resp.contains("allow")) {
                        gotoSettings(username, password);
                    } else if (resp.contains("reject")) {
                        Toast.makeText(SettingAccessActivity.this, "Invalid username or password !", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            private void gotoSettings(String username, String password) {
                Intent iSettingsActivity = new Intent(SettingAccessActivity.this, SettingsActivity.class);
                iSettingsActivity.putExtra("KEYUsername", username);
                iSettingsActivity.putExtra("KEYPassword", password);
                startActivity(iSettingsActivity);
            }
        }.execute();
    }

    private void initializeViews() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bGo = (Button) findViewById(R.id.bGo);
    }
}
