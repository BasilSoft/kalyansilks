package ks.client.classes.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ks.client.R;
import ks.client.classes.adapters.ShowItemsAdapter;
import ks.client.classes.adapters.ViewPagerAdapter;
import ks.client.classes.models.Product;
import ks.client.classes.utils.TypeListTask;

/**
 * Created by User on 7/30/2016.
 */
public class ShowItemsActivity extends AppCompatActivity implements View.OnClickListener {
    public static List<Product>[] products;
    public static RecyclerView[] rvContents;
    public static ShowItemsAdapter[] showItemsAdapters;
    public ViewPagerAdapter viewPagerAdapter;
    String pType = null;
    List<String> typeList;
    List<Product> sorted;

    int currentPage = 0;
    Button bOptions;
    ViewPager vpContent;
    TabLayout tlHead;
    EditText etSearch;

    //it is used to get state of sort such as ascending or descending
    String sortState = "ascending";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_items);

        sorted = new ArrayList<>();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            pType = extras.getString("KEYType");
        }

        initializeViews();

        if (pType.equals("gents")) {
            setupTabs(vpContent, "gents");
        } else if (pType.equals("ladies")) {
            setupTabs(vpContent, "ladies");
        } else if (pType.equals("kids")) {
            setupTabs(vpContent, "kids");
        }

        bOptions.setOnClickListener(this);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String searchText = etSearch.getText().toString();
                doSearch(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doSearch(String searchText) {
        sorted = new ArrayList<>();
        for (Product product : products[currentPage]) {
            if (product.getName().contains(searchText)) {
                sorted.add(product);
            }
        }
        showItemsAdapters[currentPage] = new ShowItemsAdapter(pType,typeList.get(currentPage),ShowItemsActivity.this, sorted);
        rvContents[currentPage].setAdapter(showItemsAdapters[currentPage]);
        showItemsAdapters[currentPage].notifyDataSetChanged();
    }

    private void initializeViews() {
        bOptions = (Button) findViewById(R.id.bOptions);
        vpContent = (ViewPager) findViewById(R.id.vpContent);
        tlHead = (TabLayout) findViewById(R.id.tlHead);
        etSearch = (EditText) findViewById(R.id.etSearch);
    }

    public void setupTabs(final ViewPager upViewpager, final String type) {
        final String url = SignInActivity.BASIC_URL + type + "?action=getTypes";

        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(url);

        final ProgressDialog loadingDialog = ProgressDialog.show(ShowItemsActivity.this, "", "Loading...", true, false);
        new AsyncTask<String, String, String>() {
            String resp = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpResponse response = client.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    resp = EntityUtils.toString(httpEntity, "utf-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loadingDialog.dismiss();
                if (resp == null) {
                    Toast.makeText(ShowItemsActivity.this, "Network error !", Toast.LENGTH_SHORT).show();
                } else {
                    typeList = new ArrayList<>();
                    try {
                        final JSONArray jaTypes = new JSONArray(resp);
                        for (int i = 0; i < jaTypes.length(); i++) {
                            final JSONObject joTypes = jaTypes.getJSONObject(i);
                            typeList.add(joTypes.getString("type"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    products = null;
                    products = new List[typeList.size()];
                    rvContents = new RecyclerView[typeList.size()];
                    showItemsAdapters = new ShowItemsAdapter[typeList.size()];

                    for (int i = 0; i < typeList.size(); i++) {
                        products[i] = new ArrayList<>();
                    }

                    viewPagerAdapter = new ViewPagerAdapter(ShowItemsActivity.this, getSupportFragmentManager(), type, typeList);
                    upViewpager.setAdapter(viewPagerAdapter);
                    upViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;
                            showItemsAdapters[position].notifyDataSetChanged();
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });

                    TypeListTask typeListTask = new TypeListTask(currentPage, ShowItemsActivity.this, typeList, type);
                    typeListTask.execute();

                    tlHead.setupWithViewPager(vpContent);
                }
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bOptions:
                showOptions();
                break;
        }
    }

    private void showOptions() {
        View anchor = bOptions;
        PopupMenu popupMenu = new PopupMenu(this, anchor);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.miName:
                        sortByName();
                        break;
                    case R.id.miPrize:
                        sortByPrize();
                        break;
                    case R.id.miPrizeInterval:
                        doIntervalSort();
                        break;
                }
                return false;
            }

            private void doIntervalSort() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ShowItemsActivity.this);
                builder.setTitle("Enter limit");
                View v = LayoutInflater.from(ShowItemsActivity.this).inflate(R.layout.layout_interval, null, false);
                builder.setView(v);
                final EditText etStart = (EditText) v.findViewById(R.id.etStart);
                final EditText etEnd = (EditText) v.findViewById(R.id.etEnd);
                final Button bSort = (Button) v.findViewById(R.id.bSort);
                final AlertDialog alert = builder.show();
                bSort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            final Integer startValue = Integer.parseInt(etStart.getText().toString());
                            final Integer endValue = Integer.parseInt(etEnd.getText().toString());

                            sorted = new ArrayList<>();
                            for (int i = 0; i < products[currentPage].size(); i++) {
                                final Product product = products[currentPage].get(i);
                                final int cost = Integer.valueOf(product.getCost());
                                if (cost >= startValue && cost <= endValue) {
                                    sorted.add(product);
                                }
                            }

                            showItemsAdapters[currentPage] = new ShowItemsAdapter(pType,typeList.get(currentPage),ShowItemsActivity.this, sorted);
                            rvContents[currentPage].setAdapter(showItemsAdapters[currentPage]);
                            showItemsAdapters[currentPage].notifyDataSetChanged();
                            alert.dismiss();
                        } catch (Exception e) {
                            Toast.makeText(ShowItemsActivity.this, "Enter inputs", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }

            private void sortByPrize() {
                if (sortState.equals("ascending")) {
                    sortState = "descending";
                    Collections.sort(products[currentPage], new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return lhs.getCost().compareToIgnoreCase(rhs.getCost());
                        }
                    });
                    Collections.sort(sorted, new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return lhs.getCost().compareToIgnoreCase(rhs.getCost());
                        }
                    });
                } else {
                    sortState = "ascending";
                    Collections.sort(products[currentPage], new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return rhs.getCost().compareToIgnoreCase(lhs.getCost());
                        }
                    });
                    Collections.sort(sorted, new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return rhs.getCost().compareToIgnoreCase(lhs.getCost());
                        }
                    });
                }
                showItemsAdapters[currentPage].notifyDataSetChanged();
            }

            private void sortByName() {
                if (sortState.equals("ascending")) {
                    sortState = "descending";
                    Collections.sort(products[currentPage]
                            , new Comparator<Product>() {
                                @Override
                                public int compare(Product lhs, Product rhs) {
                                    return lhs.getName().compareToIgnoreCase(rhs.getName());
                                }
                            });
                    Collections.sort(sorted
                            , new Comparator<Product>() {
                                @Override
                                public int compare(Product lhs, Product rhs) {
                                    return lhs.getName().compareToIgnoreCase(rhs.getName());
                                }
                            });
                } else {
                    sortState = "ascending";
                    Collections.sort(products[currentPage], new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return rhs.getName().compareToIgnoreCase(lhs.getName());
                        }
                    });
                    Collections.sort(sorted, new Comparator<Product>() {
                        @Override
                        public int compare(Product lhs, Product rhs) {
                            return rhs.getName().compareToIgnoreCase(lhs.getName());
                        }
                    });
                }
                showItemsAdapters[currentPage].notifyDataSetChanged();
            }
        });
    }
}
